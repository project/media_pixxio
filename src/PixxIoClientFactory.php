<?php

namespace Drupal\media_pixxio;

use GuzzleHttp\HandlerStack;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Factory for creating pixxio client.
 */
class PixxIoClientFactory {

  /**
   * Config factory service.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config factory.
   */
  public function __construct(ConfigFactoryInterface $config) {
    $this->config = $config->get('media_pixxio.settings');
  }

  /**
   * Return Pixxio client.
   *
   * @return PixxIoClientInterface
   *   return PixxioClient.
   */
  public function getClient(): PixxIoClientInterface {
    $stack = HandlerStack::create();
    $config = [
      'base_uri' => $this->config->get('pixxio_account_url') . '/cgi-bin/api/pixxio-api.pl/json/',
      'Content-Type' => 'application/json',
      'thumbnail_width' => ($this->config->get('pixxio_thumbnail_width')) ? $this->config->get('pixxio_thumbnail_width') : 100,
    ];

    if ($this->config->get('pixxio_auth') == 'api') {
      $config['pixxio_refresh_token'] = $this->config->get('pixxio_refresh_token');
      $config['pixxio_api_key'] = $this->config->get('pixxio_api_key');
      $tokenMiddleware = new PixxioMiddleware($config);
      $stack->push($tokenMiddleware);
    }

    $config['handler'] = $stack;
    return new PixxIoClient($config);
  }

}

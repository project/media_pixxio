<?php

namespace Drupal\media_pixxio\Exceptions;

/**
 * Bundle not found Exception.
 */
class BundleNotExistException extends \Exception {

}

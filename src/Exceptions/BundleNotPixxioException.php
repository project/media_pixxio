<?php

namespace Drupal\media_pixxio\Exceptions;

/**
 * Bundle is not Pixxio Exception.
 */
class BundleNotPixxioException extends \Exception {

}

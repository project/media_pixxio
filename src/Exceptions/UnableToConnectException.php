<?php

namespace Drupal\media_pixxio\Exceptions;

/**
 * Unable to connect Exception.
 */
class UnableToConnectException extends \Exception {

}

<?php

namespace Drupal\media_pixxio;

/**
 * Interface PixxIoClientInterface.
 */
interface PixxIoClientInterface {

  /**
   * Fetch image from pixx.io.
   *
   * @param string $url
   *   File url.
   *
   * @return array
   *   Return byte array.
   */
  public function fetchImageFile($url): array;

  /**
   * Fetch categories from pixx.io.
   *
   * @return array
   *   Return all categories.
   */
  public function fetchCategories(): array;

  /**
   * Fetch all metadata for file.
   *
   * @return \Drupal\media_pixxio\PixxioAssetData
   *   Return all categories.
   */
  public function fetchAssetData($url): PixxioAssetData;

  /**
   * Fetch all asset data.
   *
   * @param array $options
   *   Options for files @see /cgi-bin/api/pixxio-api.pl/documentation/files.
   * @param int $page
   *   Page starting by 1.
   * @param int $perPage
   *   Pager limit default by 10.
   *
   * @return array
   *   Return files found.
   */
  public function fetchFiles(array $options, $page = 1, $perPage = 10): array;

  /**
   * Return all categories.
   *
   * @return array
   *   Category structur.
   */
  public function getCategories(): array;

  /**
   * Get all defined Metaproperties.
   *
   * @return array
   *   Return all allowed Meta Properties.
   */
  public function getMetaproperties(): array;

  /**
   * Get preview url for a file.
   *
   * @return string
   *   Return preview url.
   */
  public function getPreviewUrl(array $file, $width = 100): string;

  /**
   * Return full file uri.
   *
   * @param int $id
   *   File id.
   *
   * @return string
   *   Return full uri to file.
   */
  public function getFileUri(int $id): string;

  /**
   * Return full file info.
   *
   * @param int $id
   *   File id.
   *
   * @return string
   *   Return full info to file.
   */
  public function fetchFile(int $id): array;

}

<?php

namespace Drupal\media_pixxio;

use Drupal\file\Entity\File;
use Drupal\Core\Utility\Token;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * Class FileDownloadDestinationService.
 */
class FileDownloadDestinationService implements FileDownloadDestinationServiceInterface {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Logger\LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannelMediaPixxio;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\Utility\Token definition.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * App root definition.
   *
   * @var string
   */
  protected $root;

  /**
   * Constructs a new FileDownloadDestinationService object.
   */
  public function __construct(LoggerChannelInterface $logger_channel_media_pixxio, EntityFieldManagerInterface $entity_field_manager, Token $token, EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, string $root) {
    $this->loggerChannelMediaPixxio = $logger_channel_media_pixxio;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->token = $token;
    $this->fileSystem = $file_system;
    $this->root = $root;
  }

  /**
   * Helper function for check if field type can contain a file.
   *
   * @param string $type
   *   Field type.
   *
   * @return bool
   *   Return true if is allowed.
   */
  protected static function isAllowedFileField($type): bool {
    $allowedTypes = ['image', 'file', 'video', 'audio'];
    return in_array($type, $allowedTypes);
  }

  /**
   * {@inheritdoc}
   */
  public function getDestination(EntityInterface $entity, $field_name): string {
    $field = @$this->entityFieldManager->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle())[$field_name];
    if (!$field || !self::isAllowedFileField($field->getType())) {
      throw \InvalidArgumentException($this->t('Field @name ist not a valid field type.', ['@name' => $field->getType()]));
    }
    return $field->getSetting('uri_scheme') . '://' . $this->token->replace($field->getSetting('file_directory'));
  }

  /**
   * Get default file if is defined.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   *
   * @return \Drupal\file\Entity\File
   *   Return default file if exists
   */
  public function getDefaultFile(EntityInterface $entity): File {
    $info = $this->entityTypeManager->getStorage('media_type')->load($entity->bundle())->getSource()->getConfiguration();
    if (!isset($info['default_queued_file'])) {
      throw new \Exception();
    }
    $destination = 'public://' . time() . '-' . basename($info['default_queued_file']);

    $path = $this->fileSystem->realpath($this->root . '/' . $info['default_queued_file']);
    if (!$path) {
      throw new \Exception();
    }
    $destination = $this->fileSystem->copy($path, $destination, FileSystemInterface::EXISTS_RENAME);
    $file = File::create([
      'filename' => basename($info['default_queued_file']),
      'uri' => $destination,
    ]);
    $file->setPermanent();
    $file->save();
    return $file;
  }

}

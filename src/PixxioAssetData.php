<?php

namespace Drupal\media_pixxio;

/**
 * Helper class for asset data.
 */
class PixxioAssetData {

  /**
   * Metadata.
   *
   * @var array
   */
  protected $data;

  /**
   * Constructor.
   *
   * @param array $data
   *   Array of metadata.
   */
  public function __construct(array $data) {
    $this->data = $data;
  }

  /**
   * Magic __get method to get meta property.
   *
   * @param string $name
   *   Metadata name.
   */
  public function __get($name): array {
    if (!in_array($name, $this->data)) {
      return NULL;
    }
    return @ $this->data[$name];
  }

  /**
   * Get Metadata.
   *
   * @return array
   *   Get allowed metadata.
   */
  public static function getPropertyNames(): array {
    return [
      'id' => t('ID'),
      'link' => t('Link'),
      'user' => t('User'),
      'originalFileName' => t('Original file name'),
      'fileType' => t('File Type'),
      'keyword' => t('Keywords'),
      'uploadDate' => t('Upload date'),
      'createDate' => t('Creation date'),
      'category' => t('Category'),
      'rating' => t('Rating'),
      'colormode' => t('Color mode'),
      'imagePath' => t('Image path'),
      'imageHeight' => t('Image height'),
      'imageWidth' => t('File Type'),
      'originalPath' => t('Original path'),
      'formatType' => t('Format type'),
      'subject' => t('Subject'),
      'description' => t('Description'),
      'format' => t('Format'),
      'modifyDate' => t('Modify date'),
      'fileServerName' => t('File server name'),
      'imageSize' => t('Image size'),
      'fileSize' => t('File size'),
      'orientation' => t('Orientation'),
      'gpsLat' => t('Latitute'),
      'gpsLng' => t('Longitute'),
      'gpsAlt' => t('Alteration'),
      'gpsInfo' => t('GPS Info'),
      'checkOut' => t('Checkout'),
      'modifiedImagePaths' => t('Modified image paths'),
      'clipPath' => t('Clip path'),
      'fileStatus' => t('Status'),
      'collections' => t('Collections'),
      'exifData' => t('Exif data'),
      'md5sum' => t('MD5 sum'),
      'keywordsRecognition' => t('Keywords recognition'),
      'dynamicMetadata' => t('Dynamic metadata'),
      'licenses' => t('Licences'),
      'modelProperties' => t('Model properties'),
      'publicUri' => t('Public uri'),
      'previewFile' => t('Preview File'),
      'webImage' => t('Converted image file'),
    ];
  }

}

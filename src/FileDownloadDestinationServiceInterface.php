<?php

namespace Drupal\media_pixxio;

use Drupal\file\Entity\File;
use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for FileDownloadDestinationService.
 */
interface FileDownloadDestinationServiceInterface {

  /**
   * Return destination from file settings.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to check.
   * @param string $field_name
   *   Field name to check. Must be an file field.
   *
   * @return string
   *   URI to default destination.
   */
  public function getDestination(EntityInterface $entity, $field_name): string;

  /**
   * Return default file uri.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   Return managed file for entity.
   *
   * @return Drupal\file\Entity\File
   *   Return file to default.
   */
  public function getDefaultFile(EntityInterface $entity): File;

}

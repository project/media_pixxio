<?php

namespace Drupal\media_pixxio;

/**
 * Guzzle Middleware.
 */
use Psr\Http\Message\RequestInterface;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Middleware;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Client;

/**
 * Guzzle Middleware.
 */
class PixxioMiddleware {

  /**
   * Access token.
   *
   * @var string
   */
  protected $token = NULL;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * Api key for pixxio.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * Refreh token.
   *
   * @var string
   */
  protected $refreshToken;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $config) {
    $config['handler'] = HandlerStack::create();
    $this->apiKey = $config['pixxio_api_key'];
    $this->refreshToken = $config['pixxio_refresh_token'];
    $this->client = new Client($config);
  }

  /**
   * {@inheritdoc}
   */
  public function __invoke(callable $next) {
    return function (RequestInterface $request, array $options = []) use ($next) {
      $request = $this->applyToken($request);
      return $next($request, $options);
    };
  }

  /**
   * {@inheritdoc}
   */
  public static function create(array $params) {
    return Middleware::mapRequest(new static($params));
  }

  /**
   * Apply token to all requests.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   Current request.
   *
   * @return \Psr\Http\Message\RequestInterface
   *   Modified request.
   */
  protected function applyToken(RequestInterface $request): RequestInterface {
    if (!$this->hasValidToken()) {
      $this->acquireAccessToken();
    }
    return $request->withUri(Uri::withQueryValue(
        $request->getUri(),
        'accessToken',
        $this->getToken()
    ));
  }

  /**
   * Get access token.
   *
   * @return string
   *   Return Access Token.
   */
  public function getToken(): ?string {
    return $this->token;
  }

  /**
   * Check if there is a valid token.
   *
   * @return bool
   *   Is a valid token.
   */
  public function hasValidToken(): bool {
    return !(NULL === $this->token);
  }

  /**
   * Fetch new access token.
   */
  public function acquireAccessToken(): void {
    $response = $this->client->post('accessToken', [
      'json' => [
        'apiKey' => $this->apiKey,
        'refreshToken' => $this->refreshToken,
      ],
    ]);
    $response = \GuzzleHttp\json_decode((string) $response->getBody(), TRUE);
    if (!isset($response['accessToken'])) {
      $this->token = NULL;
      return;
    }
    $this->token = $response['accessToken'];
  }

}

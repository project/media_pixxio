<?php

namespace Drupal\media_pixxio\Plugin\EntityBrowser\Widget;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\media_pixxio\Plugin\media\Source\Pixxio;
use Drupal\media_pixxio\PixxIoClientInterface;
use Drupal\media_pixxio\Exceptions\UnableToConnectException;
use Drupal\media\MediaInterface;
use Drupal\media\Entity\Media;
use Drupal\entity_browser\WidgetValidationManager;
use Drupal\entity_browser\Element\EntityBrowserPagerElement;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Component\Datetime\TimeInterface;

/**
 * Uses a Pixxio API to search and provide entity listing in a browser's widget.
 *
 * @EntityBrowserWidget(
 *   id = "pixxio_search",
 *   label = @Translation("Pixxio search"),
 *   description = @Translation("Adds an Pixxio search field browser's widget.")
 * )
 */
class MediaPixxio extends MediaPixxioBase {

  /**
   * Limits the amount of tags returned in the Media browser filter.
   */
  const TAG_LIST_LIMIT = 25;

  /**
   * Account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $accountProxy;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity query factory.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * The cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * MediaPixxio constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\entity_browser\WidgetValidationManager $validation_manager
   *   The Widget Validation Manager service.
   * @param \Drupal\media_pixxio\PixxIoClientInterface $api
   *   Pixxio API service.
   * @param \Drupal\Core\Session\AccountProxyInterface $account_proxy
   *   Account proxy.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   Url generator.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The entity query factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EventDispatcherInterface $event_dispatcher,
    EntityTypeManagerInterface $entity_type_manager,
    WidgetValidationManager $validation_manager,
    PixxIoClientInterface $api,
    AccountProxyInterface $account_proxy,
    UrlGeneratorInterface $url_generator,
    LoggerChannelFactoryInterface $logger_factory,
    LanguageManagerInterface $language_manager,
    RequestStack $request_stack,
    QueryFactory $entity_query,
    ConfigFactoryInterface $config_factory,
    CacheBackendInterface $cache,
    TimeInterface $time,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher, $entity_type_manager, $validation_manager, $api, $logger_factory, $language_manager, $request_stack, $config_factory);
    $this->accountProxy = $account_proxy;
    $this->urlGenerator = $url_generator;
    $this->entityQuery = $entity_query;
    $this->cache = $cache;
    $this->time = $time;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_browser.widget_validation'),
      $container->get('media_pixxio.client'),
      $container->get('current_user'),
      $container->get('url_generator'),
      $container->get('logger.factory'),
      $container->get('language_manager'),
      $container->get('request_stack'),
      $container->get('entity.query'),
      $container->get('config.factory'),
      $container->get('cache.data'),
      $container->get('datetime.time'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'items_per_page' => 15,
      'tags_filter' => TRUE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['items_per_page'] = [
      '#type' => 'select',
      '#title' => $this->t('Items per page'),
      '#default_value' => $this->configuration['items_per_page'],
      '#options' => ['10' => 10, '15' => 15, '25' => 25, '50' => 50],
    ];

    $form['tags_filter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable tags filter'),
      '#default_value' => $this->configuration['tags_filter'],
    ];

    foreach ($this->entityTypeManager->getStorage('media_type')->loadMultiple() as $type) {
      /** @var \Drupal\media\MediaTypeInterface $type */
      if ($type->getSource() instanceof Pixxio) {
        $form['media_type']['#options'][$type->id()] = $type->label();
      }
    }

    if (empty($form['media_type']['#options'])) {
      $form['media_type']['#disabled'] = TRUE;
      $form['items_per_page']['#disabled'] = TRUE;
      $form['media_type']['#description'] = $this->t('You must @create_type before using this widget.', [
        '@create_type' => Link::createFromRoute($this->t('create a Pixxio media type'), 'entity.media_type.add_form')
          ->toString(),
      ]);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEntities(array $form, FormStateInterface $form_state) {
    if (!$this->checkType()) {
      return [];
    }
    $media = [];
    $selected_ids = array_keys(array_filter($form_state->getValue('selection', [])));
    /** @var \Drupal\media\MediaTypeInterface $type */
    $type = $this->entityTypeManager->getStorage('media_type')
      ->load($this->configuration['media_type']);
    $plugin = $type->getSource();
    $source_field = $plugin->getConfiguration()['source_field'];
    foreach ($selected_ids as $api_id) {
      $mid = $this->entityQuery->get('media')
        ->condition($source_field, $api_id)
        ->range(0, 1)
        ->execute();
      if ($mid) {
        $media[] = $this->entityTypeManager->getStorage('media')
          ->load(reset($mid));
      }
      else {
        $media[] = Media::create([
          'bundle' => $type->id(),
          $source_field => $api_id,
        ]);
      }
    }
    return $media;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);

    if ($form_state->getValue('errors')) {
      $form['actions']['submit']['#access'] = FALSE;
      return $form;
    }

    $form['#attached']['library'][] = 'media_pixxio/search_view';

    $form['filters']['search_pixxio'] = [
      '#type' => 'textfield',
      '#weight' => -1,
      '#title' => $this->t('Search keyword'),
      '#attributes' => [
        'size' => 30,
      ],
    ];

    $max_option_weight = 0;

    try {
      // We store last result into the form state to prevent same requests
      // happening multiple times if not necessary.
      if (!empty($form_state->get('pixxio_media_tags'))) {
        $tag_list = $form_state->get('pixxio_media_tags');
      }
      else {
        $tag_list = $this->api->getCategories();
        $form_state->set('pixxio_media_tags', $tag_list);
      }
      $options = [
        '_all' => $this->t('All categories'),
      ];
      foreach ($tag_list['categories'] as $value) {
        $name = explode('/', $value);
        $name = array_filter($name);
        $name = implode('	» ', $name);
        $options[$value] = $name;
      }
      $form['filters']['category'] = [
        '#type' => 'select',
        '#weight' => 1,
        '#title' => $this->t('Search in category'),
        '#options' => $options,
      ];
    }
    catch (\Exception $e) {
      (new UnableToConnectException())->logException()->displayMessage();
      $form['actions']['submit']['#access'] = FALSE;
      return $form;
    }

    $form['search_button'] = [
      '#type' => 'button',
      '#weight' => $max_option_weight + 10,
      '#value' => $this->t('Search'),
      '#name' => 'search_submit',
    ];

    $form['thumbnails'] = [
      '#type' => 'container',
      '#weight' => $max_option_weight + 15,
      '#attributes' => ['id' => 'thumbnails', 'class' => 'grid'],
    ];

    if ($form_state->getTriggeringElement()['#name'] == 'search_submit') {
      EntityBrowserPagerElement::setCurrentPage($form_state);
    }
    $page = EntityBrowserPagerElement::getCurrentPage($form_state);

    $query = [];

    if ($form_state->getValue('search_pixxio')) {
      $query['searchTerm'] = $form_state->getValue('search_pixxio');
    }
    if ($form_state->getValue('category') && $form_state->getValue('category') != '_all') {
      $query['category'] = $form_state->getValue('category');
    }
    if (!in_array('_all', $this->configuration['pixxio_type'])) {
      $query['formatType'] = $this->configuration['pixxio_type'];
    }

    try {
      // We store last result into the form state to prevent same requests
      // happening multiple times if not necessary.
      $media_list = $this->api->fetchFiles($query, $page, $this->configuration['items_per_page']);
      $form_state->set('pixxio_media_list', $media_list);
    }
    catch (\Exception $e) {
      (new UnableToConnectException())->logException()->displayMessage();
      $form['actions']['submit']['#access'] = FALSE;
      return $form;
    }

    if (!empty($media_list['files'])) {
      foreach ($media_list['files'] as $media) {
        $form['thumbnails']['thumbnail-' . $media['id']] = [
          '#type' => 'container',
          '#attributes' => ['id' => $media['id'], 'class' => ['grid-item']],
        ];
        $form['thumbnails']['thumbnail-' . $media['id']]['check_' . $media['id']] = [
          '#type' => 'checkbox',
          '#parents' => ['selection', $this->api->getFileUri($media['id'])],
          '#attributes' => ['class' => ['item-selector']],
        ];
        $form['thumbnails']['thumbnail-' . $media['id']]['image'] = [
          '#theme' => 'media_pixxio_search_item',
          '#thumbnail_uri' => $this->api->getPreviewUrl($media, 200),
          '#name' => $media['subject'],
          '#type' => $media['formatType'],
        ];
      }

      $form['pager_eb'] = [
        '#type' => 'entity_browser_pager',
        '#total_pages' => (int) ceil($media_list['quantity'] / $this->configuration['items_per_page']),
        '#weight' => $max_option_weight + 15,
      ];

      // Set validation errors limit to prevent validation of filters on select.
      // We also need to set #submit to the default submit callback otherwise
      // limit won't take effect. Thank you Form API, you are very kind...
      // @see \Drupal\Core\Form\FormValidator::determineLimitValidationErrors()
      $form['actions']['submit']['#limit_validation_errors'] = [['selection']];
      $form['actions']['submit']['#submit'] = ['::submitForm'];
    }
    else {
      $form['empty_message'] = [
        '#prefix' => '<div class="empty-message">',
        '#markup' => $this->t('Not assets found for current search criteria.'),
        '#suffix' => '</div>',
        '#weight' => $max_option_weight + 20,
      ];
      $form['actions']['submit']['#access'] = FALSE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array &$element, array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getTriggeringElement()['#eb_widget_main_submit'])) {
      try {
        /*
         * We store info about media assets that we already fetched so the media
         * entity plugin can use them and avoid doing more API requests.
         *
         * @see \Drupal\media_pixxo\Plugin\MediaEntity\Type\Pixxio::getField()
         */
        $selected_ids = array_keys(array_filter($form_state->getValue('selection', [])));
        if ($api_list = $form_state->get('pixxio_media_list')) {
          foreach ($api_list['media'] as $api_item) {
            foreach ($selected_ids as $selected_id) {
              if ($api_item['id'] == $selected_id) {
                $this->cache->set('pixxio_item_' . $selected_id, $api_item, ($this->time->getRequestTime() + 120));
              }
            }
          }
        }

        $media = $this->prepareEntities($form, $form_state);
        array_walk($media, function (MediaInterface $media_item) {
          $media_item->save();
        });
        $this->selectEntities($media, $form_state);
      }
      catch (\UnexpectedValueException $e) {
        $this->messenger()->addError($this->t('Pixxio integration is not configured correctly. Please contact the site administrator.'));
      }
    }
  }

}

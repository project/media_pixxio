<?php

namespace Drupal\media_pixxio\Plugin\EntityBrowser\Widget;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\media_pixxio\Plugin\media\Source\Pixxio;
use Drupal\media_pixxio\PixxIoClientInterface;
use Drupal\media_pixxio\Exceptions\BundleNotPixxioException;
use Drupal\media_pixxio\Exceptions\BundleNotExistException;
use Drupal\entity_browser\WidgetValidationManager;
use Drupal\entity_browser\WidgetBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Base class for Pixxio Entity browser widgets.
 */
abstract class MediaPixxioBase extends WidgetBase {

  /**
   * Pixxio API service.
   *
   * @var \Drupal\media_pixxio\PixxIoClientInterface
   */
  protected $api;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * MediaPixxioBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\entity_browser\WidgetValidationManager $validation_manager
   *   The Widget Validation Manager service.
   * @param \Drupal\media_pixxio\PixxIoClientInterface $api
   *   Pixxio API service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EventDispatcherInterface $event_dispatcher, EntityTypeManagerInterface $entity_type_manager, WidgetValidationManager $validation_manager, PixxIoClientInterface $api, LoggerChannelFactoryInterface $logger_factory, LanguageManagerInterface $language_manager, RequestStack $request_stack, ConfigFactoryInterface $config_factory) {
    $this->api = $api;
    $this->loggerFactory = $logger_factory;
    $this->languageManager = $language_manager;
    $this->requestStack = $request_stack;
    $this->config = $config_factory;

    parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher, $entity_type_manager, $validation_manager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_browser.widget_validation'),
      $container->get('media_pixxio.client'),
      $container->get('logger.factory'),
      $container->get('language_manager'),
      $container->get('request_stack'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'media_type' => NULL,
      'pixxio_type' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Media type'),
      '#default_value' => $this->configuration['media_type'],
      '#required' => TRUE,
      '#options' => [],
    ];

    $form['pixxio_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Pixxio type'),
      '#default_value' => $this->configuration['pixxio_type'],
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#options' => [
        '_all' => $this->t('All formats'),
        'webimage' => $this->t('Webimage'),
        'raw' => $this->t('Raw'),
        'video' => $this->t('Video'),
        'audio' => $this->t('Audio'),
        'office' => $this->t('Office'),
        'other' => $this->t('Other'),
        'image' => $this->t('All image types'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);

    if (!$this->checkType()) {
      $form_state->setValue('errors', TRUE);
      return $form;
    }
    return $form;
  }

  /**
   * Check that media type is properly configured.
   *
   * @return bool
   *   Returns TRUE if media type is configured correctly.
   */
  protected function checkType() {
    /** @var \Drupal\media\MediaTypeInterface $type */
    $type = $this->entityTypeManager->getStorage('media_type')
      ->load($this->configuration['media_type']);

    if (!$type) {
      (new BundleNotExistException(
        $this->configuration['media_type']
      ))->logException()->displayMessage();
      return FALSE;
    }
    elseif (!($type->getSource() instanceof Pixxio)) {
      (new BundleNotPixxioException($type->label()))->logException()
        ->displayMessage();
      return FALSE;
    }
    return TRUE;
  }

}

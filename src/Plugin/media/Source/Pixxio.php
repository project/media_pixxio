<?php

namespace Drupal\media_pixxio\Plugin\media\Source;

use Drupal\file\Entity\File;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\Core\Queue\QueueFactory;
use Drupal\media\MediaTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media_pixxio\PixxioAssetData;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Render\RendererInterface;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\media_pixxio\PixxIoClientInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\media_pixxio\FileDownloadDestinationServiceInterface;

/**
 * Media source plugin.
 *
 * @MediaSource(
 *   id = "pixxio",
 *   label = @Translation("Pixx.io"),
 *   allowed_field_types = {"string", "string_long"},
 *   default_thumbnail_filename = "pixxio-logo.png",
 *   description = @Translation("Provides business logic and metadata for gists."),
 * )
 */
class Pixxio extends MediaSourceBase {
  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The git fetcher.
   *
   * @var \Drupal\media_pixxio\PixxIoClientInterface
   */
  protected $api;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Statically cached API response for a given asset.
   *
   * @var array
   */
  protected $apiResponse;

  /**
   * The cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The queue service.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * File destination service.
   *
   * @var \Drupal\media_pixxio\FileDownloadDestinationServiceInterface
   */
  protected $fileDownloadService;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   Config field type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\media_pixxio\PixxIoClientInterface $api
   *   The git fetcher.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   Queue service.
   * @param \Drupal\media_pixxio\FileDownloadDestinationServiceInterface $file_download_service
   *   File download serve.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File system serve.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    FieldTypePluginManagerInterface $field_type_manager,
    ConfigFactoryInterface $config_factory,
    RendererInterface $renderer,
    PixxIoClientInterface $api,
    LoggerChannelInterface $logger,
    CacheBackendInterface $cache,
    QueueFactory $queue,
    FileDownloadDestinationServiceInterface $file_download_service,
    FileSystemInterface $file_system
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->renderer = $renderer;
    $this->api = $api;
    $this->logger = $logger;
    $this->cache = $cache;
    $this->queue = $queue->get('pixxio_download_queue');
    $this->fileDownloadService = $file_download_service;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('renderer'),
      $container->get('media_pixxio.client'),
      $container->get('logger.channel.media_pixxio'),
      $container->get('cache.data'),
      $container->get('queue'),
      $container->get('media_pixxio.download_destination'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'source_field' => '',
      'default_queued_file' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['default_queued_file'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Set a file for queued downloads as preview. Relative from docroot.'),
      '#default_value' => (isset($this->configuration['default_queued_file'])) ? $this->configuration['default_queued_file'] : '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return PixxioAssetData::getPropertyNames();
  }

  /**
   * Helper function for check if field type can contain a file.
   *
   * @param string $type
   *   Field type.
   *
   * @return bool
   *   Return true if is allowed.
   */
  protected static function isAllowedFileField($type): bool {
    $allowedTypes = ['image', 'file', 'video', 'audio'];
    return in_array($type, $allowedTypes);
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $name) {
    if (!$source_field = $this->configuration['source_field']) {
      return FALSE;
    }

    if (!$media_uuid = $media->{$source_field}->value) {
      return FALSE;
    }

    if ($name == 'uuid') {
      return $media_uuid;
    }

    $apiId = explode('/', $media_uuid);
    $apiId = end($apiId);

    if (!isset($this->apiResponse)) {
      try {
        // Check for cached data first, either set below or by
        // to avoid extra API requests.
        if ($cache = $this->cache->get('pixxo:item:' . $apiId)) {
          $this->apiResponse = $cache->data;
        }
        // If there is no catch, try to fetch data if no previous request failed
        // with a timeout.
        else {
          $this->apiResponse = $this->api->fetchFile($apiId);
          // Cache the response for the configured timeframe.
          $this->cache->set('pixxo:item:' . $apiId, $this->apiResponse, time() + 10);
        }
      }
      catch (GuzzleException $e) {
        $this->logger->get('pixxio_media')->error('Unable to fetch info about the asset represented by media @name (@id) with message @message.', [
          '@name' => $media->label(),
          '@id' => $media->id(),
          '@message' => $e->getMessage(),
        ]);
        return FALSE;
      }
    }
    if (!empty($this->apiResponse)) {
      $isImage = FALSE;
      $format = $this->apiResponse['format'];
      $format = explode('/', $format);
      if ($format[0] == 'image') {
        $isImage = TRUE;
      }
      switch ($name) {
        case 'default_name':
          return $this->apiResponse['subject'];

        case 'originalPath':
          $map = $this->entityTypeManager->getStorage('media_type')->load($media->bundle())->getFieldMap();
          if (!isset($map['originalPath'])) {
            $this->logger->info('No mapping for file');
            return $this->apiResponse['originalPath'];
          }
          if (!self::isAllowedFileField($media->get($map['originalPath'])->getFieldDefinition()->getType())) {
            $this->logger->info('Not a allowed filed find');
            return $this->apiResponse['originalPath'];
          }
          if (!$media->get($map['originalPath'])->isEmpty()) {
            return $media->get($map['originalPath'])->first()->entity;
          }
          $fileSize = $this->getMetadata($media, 'fileSize');

          if (self::shouldQueued($fileSize)) {
            $this->logger->info('Create a queued download for @file');
            $this->queue->createItem([
              'id' => $apiId,
              'file' => $this->apiResponse['originalPath'],
              'field' => $map['originalPath'],
              'name' => $this->apiResponse['originalFilename'],
              'bundle' => $media->bundle(),
              'media' => $media_uuid,
            ]);
            $this->logger->info('Try to create default field');
            try {
              $file = $this->fileDownloadService->getDefaultFile($media);
              $this->logger->info('Added default file');
              return $file;
            }
            catch (\Exception $e) {
              $this->logger->info('Could not create default file ' . $e->getMessage());
              return $this->apiResponse['originalPath'];
            }
          }
          $fileName = $this->getMetadata($media, 'originalFilename');
          $url = $this->apiResponse['originalPath'];
          return $this->saveFile($url, $fileName, $map[$name], $media);

        case 'webImage':
          if (!$isImage) {
            return NULL;
          }
          $map = $this->entityTypeManager->getStorage('media_type')->load($media->bundle())->getFieldMap();
          $fileName = $this->getMetadata($media, 'originalFilename');
          if (!isset($map[$name])) {
            $this->logger->info('No mapping for file');
            return $this->apiResponse[$name];
          }
          if (!self::isAllowedFileField($media->get($map[$name])->getFieldDefinition()->getType())) {
            $this->logger->info('Not a allowed filed find');
            return $this->apiResponse['originalPath'];
          }
          if (!$media->get($map[$name])->isEmpty()) {
            return $media->get($map[$name])->first()->entity;
          }
          $fileSize = $this->getMetadata($media, 'fileSize');
          $url = $this->apiResponse['originalPath'];

          if (!self::isWebImage($this->getMetadata($media, 'fileType'))) {
            $url = $this->api->getPreviewUrl($this->apiResponse, $this->getMetadata($media, 'imageWidth'));
            $tmpUrl = explode('.', $url);
            $tmpUrl = end($tmpUrl);
            $fileName = explode('.', $fileName);
            array_pop($fileName);
            $fileName[] = $tmpUrl;
            $fileName = implode('.', $fileName);
          }
          return $this->saveFile($url, $fileName, $map[$name], $media);

        case 'thumbnail_uri':
          if ($isImage) {
            $file = $this->getMetadata($media, 'webImage');
            if ($file instanceof File) {
              return $file->getFileUri();
            }
          }
          $file = $this->getMetadata($media, 'previewFile');
          if ($file instanceof File) {
            return $file->getFileUri();
          }
          return '';

        case 'previewFile':
          $map = $this->entityTypeManager->getStorage('media_type')->load($media->bundle())->getFieldMap();
          if (!isset($map[$name])) {
            return $this->apiResponse[$name];
          }
          if (!self::isAllowedFileField($media->get($map[$name])->getFieldDefinition()->getType())) {
            return $this->apiResponse[$name];
          }
          if (!$media->get($map[$name])->isEmpty()) {
            return $media->get($map[$name])->first()->entity;
          }
          $url = $this->api->getPreviewUrl($this->apiResponse);
          $ext = explode('.', $url);
          $dest = 'thumb-' . $apiId . '.' . end($ext);
          return $this->saveFile($url, $dest, $map[$name], $media);

        default:
          return $this->apiResponse[$name];

      }
    }

    return NULL;
  }

  /**
   * Check if download should queued.
   *
   * @param int $size
   *   Size to check.
   *
   * @return bool
   *   Return if should be queued.
   */
  protected function shouldQueued($size): bool {
    return ($size > 36899180);
  }

  /**
   * Check if is a web image.
   *
   * @param string $type
   *   Type to check.
   *
   * @return bool
   *   Return if should be queued.
   */
  protected static function isWebImage($type): bool {
    $allowedTypes = ['jpg', 'png', 'gif'];
    return in_array(strtolower($type), $allowedTypes);
  }

  /**
   * Helper function for retrieve file.
   *
   * @param string $url
   *   Url to retrieve.
   * @param string $name
   *   Destination name to save.
   * @param string $field_name
   *   Field name for destination.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Media entity.
   *
   * @return \Drupal\file\Entity\File
   *   return File entity.
   */
  protected function saveFile($url, $name, $field_name, EntityInterface $entity): File {
    $directory = 'public://pixxio';
    try {
      $directory = $this->fileDownloadService->getDestination($entity, $field_name);
    }
    catch (\InvalidArgumentException $ex) {
      $this->logger->error($ex->getMessage());
    }
    if (!file_exists($directory)) {
      $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    }
    return system_retrieve_file($url, $directory . '/' . $name, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    return parent::createSourceField($type)->set('label', 'Pixx.io ID');
  }

  /**
   * Get the primary value stored in the source field.
   *
   * @todo This helper method was added to MediaSourceBase in 8.5.0 but we
   * replicate it here because we want to support 8.4.0 sites as well. This
   * method can be safely removed once there is no need to support 8.4 anymore,
   * and we ensure the core Media dependency is bumped to 8.5.0 at least.
   *
   * @param \Drupal\media\MediaInterface $media
   *   A media item.
   *
   * @return mixed
   *   The source value.
   *
   * @throws \RuntimeException
   *   If the source field for the media source is not defined.
   */
  public function getSourceFieldValue(MediaInterface $media) {
    $source_field = $this->configuration['source_field'];
    if (empty($source_field)) {
      throw new \RuntimeException('Source field for media source is not defined.');
    }

    /** @var \Drupal\Core\Field\FieldItemInterface $field_item */
    $field_item = $media->get($source_field)->first();
    return $field_item->{$field_item->mainPropertyName()};
  }

}

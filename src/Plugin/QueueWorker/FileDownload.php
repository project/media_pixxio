<?php

namespace Drupal\media_pixxio\Plugin\QueueWorker;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\media_pixxio\PixxIoClientInterface;
use Drupal\file\Entity\File;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\media_pixxio\FileDownloadDestinationServiceInterface;

/**
 * Processes download for large files.
 *
 * @QueueWorker(
 *   id = "pixxio_download_queue",
 *   title = @Translation("Downloader for large files"),
 *   cron = {"time" = 240}
 * )
 */
class FileDownload extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Pixxio client.
   *
   * @var \Drupal\media_pixxio\PixxIoClientInterface
   */
  protected $client;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * File download destination.
   *
   * @var \Drupal\media_pixxio\FileDownloadDestinationServiceInterface
   */
  protected $fileDownloadService;

  /**
   * File download destination.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    PixxIoClientInterface $client,
    QueryFactory $entity_query,
    FileDownloadDestinationServiceInterface $file_download_destination,
    FileSystemInterface $file_system
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->client = $client;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityQuery = $entity_query;
    $this->fileDownloadService = $file_download_destination;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('media_pixxio.client'),
      $container->get('entity.query'),
      $container->get('media_pixxio.download_destination'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $type = $this->entityTypeManager->getStorage('media_type')->load($data['bundle']);
    $plugin = $type->getSource();
    $source_field = $plugin->getConfiguration()['source_field'];
    $query = $this->entityQuery->get('media');

    $result = $query->condition($source_field, $data['media'])->execute();
    if (!$result) {
      return;
    }
    $medias = $this->entityTypeManager->getStorage('media')->loadMultiple(array_values($result));

    foreach ($medias as $media) {
      try {
        $file = $this->saveFile($data['file'], $data['name'], $media, $data['field']);
        if ($file) {
          $media->set($data['field'], [
            'target_id' => $file->id(),
          ]);
          $media->save();
        }
      }
      catch (\Exception $ex) {
        throw new SuspendQueueException();
      }
    }
    return TRUE;
  }

  /**
   * Helper function for save file.
   *
   * @param string $url
   *   Url to pixxio file.
   * @param string $name
   *   Original filename.
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   Media entity.
   * @param string $field_name
   *   Field name.
   *
   * @return \Drupal\file\Entity\File
   *   File entity.
   */
  protected function saveFile($url, $name, EntityInterface $entity, $field_name): File {
    $directory = 'public://pixxio';
    try {
      $directory = $this->fileDownloadService->getDestination($entity, $field_name);
    }
    catch (\InvalidArgumentException $ex) {
      throw new \Exception();
    }
    if (!file_exists($directory)) {
      $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    }
    $file = system_retrieve_file($url, $directory . '/' . $name, TRUE);
    if (!$file) {
      throw new \Exception('Could not download.');
    }
    return $file;
  }

}

<?php

namespace Drupal\media_pixxio\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class PixxioSettingsForm extends ConfigFormBase {

  /**
   * Config id.
   *
   * @var string Config settings
  */
  const SETTINGS = 'media_pixxio.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pixxio_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['pixxio_account_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Pixx.io account url'),
      '#default_value' => $config->get('pixxio_account_url'),
    ];

    $form['pixxio_auth'] = [
      '#type' => 'radios',
      '#options' => [
        'api' => $this->t('API Key'),
        'oauth' => $this->t('OAuth'),
      ],
      '#default_value' => $config->get('pixxio_auth') ? $config->get('pixxio_auth') : 'api',
    ];

    $form['pixxio_refresh_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pixx.io refresh token'),
      '#default_value' => $config->get('pixxio_refresh_token'),
      '#states' => [
        'invisible' => [
          ':input[name="pixxio_auth"]' => [
            'value' => 'oauth',
          ],
        ],
      ],
    ];

    $form['pixxio_refresh_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pixx.io refresh token'),
      '#default_value' => $config->get('pixxio_refresh_token'),
      '#states' => [
        'invisible' => [
          ':input[name="pixxio_auth"]' => [
            'value' => 'oauth',
          ],
        ],
      ],
    ];

    $form['pixxio_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Api key'),
      '#default_value' => $config->get('pixxio_api_key'),
      '#states' => [
        'invisible' => [
          ':input[name="pixxio_auth"]' => [
            'value' => 'oauth',
          ],
        ],
      ],
    ];

    $form['pixxio_api_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client id'),
      '#default_value' => $config->get('pixxio_api_client_id'),
      '#states' => [
        'invisible' => [
          ':input[name="pixxio_auth"]' => [
            'value' => 'api',
          ],
        ],
      ],
    ];

    $form['pixxio_api_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret'),
      '#default_value' => $config->get('pixxio_api_client_secret'),
      '#states' => [
        'invisible' => [
          ':input[name="pixxio_auth"]' => [
            'value' => 'api',
          ],
        ],
      ],
    ];

    $form['pixxio_thumbnail_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Thumbnail width'),
      '#default_value' => $config->get('pixxio_thumbnail_width') ? $config->get('pixxio_thumbnail_width') : 100,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('pixxio_account_url', $form_state->getValue('pixxio_account_url'))
      ->set('pixxio_refresh_token', $form_state->getValue('pixxio_refresh_token'))
      ->set('pixxio_auth', $form_state->getValue('pixxio_auth'))
      ->set('pixxio_api_key', $form_state->getValue('pixxio_api_key'))
      ->set('pixxio_thumbnail_width', $form_state->getValue('pixxio_thumbnail_width'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

<?php

namespace Drupal\media_pixxio;

use GuzzleHttp\Client;
use Drupal\Component\Serialization\Json;

/**
 * Class PixxIoClient.
 */
class PixxIoClient extends Client implements PixxIoClientInterface {

  /**
   * Api key.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * Refresh token.
   *
   * @var string
   */
  protected $refreshToken;

  /**
   * Refresh token.
   *
   * @var int
   */
  protected $thumbnailWidth;

  /**
   * Constructs a new PixxIoClient object.
   */
  public function __construct(array $config) {
    $this->thumbnailWidth = $config['thumbnail_width'];
    parent::__construct($config);
  }

  /**
   * Fetch Files.
   *
   * @param array $options
   *   Options for files.
   * @param int $page
   *   Page starting by 1.
   * @param int $perPage
   *   Offset pager, default by 10.
   *
   * @return array
   *   Return files.
   */
  public function fetchFiles(array $options, $page = 1, $perPage = 10): array {
    $options = array_merge($options, ['pagination' => "${perPage}-${page}"]);
    $response = $this->get('files', [
      'query' => ['options' => Json::encode($options)],
    ]);
    return Json::decode($response->getBody());
  }

  /**
   * {@inheritdoc}
   */
  public function getCategories(): array {
    $response = $this->get('categories');
    return Json::decode($response->getBody());
  }

  /**
   * {@inheritdoc}
   */
  public function fetchImageFile($url): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function fetchCategories(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetaproperties(): array {
    return PixxioAssetData::getPropertyNames();
  }

  /**
   * {@inheritdoc}
   */
  public function fetchAssetData($url): PixxioAssetData {
    return new PixxioAssetData([]);
  }

  /**
   * Return preview url for file.
   *
   * @param array $file
   *   File info.
   * @param int $width
   *   Width for the image.
   *
   * @return string
   *   Return uri to preview url.
   */
  public function getPreviewUrl(array $file, $width = 100): string {
    return preg_replace('/w=\d*/', 'w=' . $width, $file['imagePath']);
  }

  /**
   * Get file uri.
   *
   * @param int $id
   *   File id.
   *
   * @return string
   *   Return full uri to file.
   */
  public function getFileUri(int $id): string {
    return $this->getConfig('base_uri') . 'files/' . $id;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchFile(int $id): array {
    $response = $this->get('files/' . $id);
    return Json::decode($response->getBody());
  }

}
